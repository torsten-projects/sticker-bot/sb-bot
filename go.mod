module sticker-bot

go 1.18

require (
	github.com/anthonynsimon/bild v0.13.0
	github.com/fogleman/gg v1.3.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/nxshock/colorcrop v0.0.0-20210323183931-9fb5e5006ee3
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_golang v1.12.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.32.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	gocv.io/x/gocv v0.30.0 // indirect
	golang.org/x/image v0.0.0-20220413100746-70e8d0d3baa9 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	gonum.org/v1/gonum v0.11.0 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)

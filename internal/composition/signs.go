package composition

var BakaFreem Composition = Composition{
	"_sources/freem_baka.png",
	[4][2]float64{
		{72, 189},
		{385, 246},
		{338, 487},
		{23, 425},
	},
	[2]float64{15, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var BakaFreemRoboto Composition = Composition{
	"_sources/freem_baka.png",
	[4][2]float64{
		{72.0, 189.0},
		{385.0, 246.0},
		{338.0, 487.0},
		{23.0, 425.0},
	},
	[2]float64{15, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var BakaOggy Composition = Composition{
	"_sources/oggy_baka.png",
	[4][2]float64{
		{81.0, 176.0},
		{401.0, 236.0},
		{353.0, 483.0},
		{30.0, 420.0},
	},
	[2]float64{15, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var BakaAzshara Composition = Composition{
	"_sources/azshara_baka.png",
	[4][2]float64{
		{71.0, 172.0},
		{404.0, 235.0},
		{354.0, 491.0},
		{19.0, 425.0},
	},
	[2]float64{15, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var BakaDbleki Composition = Composition{
	"_sources/dbleki_baka.png",
	[4][2]float64{
		{71.0, 172.0},
		{404.0, 235.0},
		{354.0, 491.0},
		{19.0, 425.0},
	},
	[2]float64{15, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var SignRadar Composition = Composition{
	"_sources/radar_sign.png",
	[4][2]float64{
		{79.0, 328.0},
		{343.0, 327.0},
		{343.0, 501.0},
		{88.0, 492.0},
	},
	[2]float64{5, 15},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var BakaDaxter Composition = Composition{
	"_sources/daxter_baka.png",
	[4][2]float64{
		{79.0, 178.0},
		{383.0, 235.0},
		{340.0, 472.0},
		{34.0, 411.0},
	},
	[2]float64{15, 7},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var SignDusk Composition = Composition{
	"_sources/dusk_sign.png",
	[4][2]float64{
		{108.0, 307.0},
		{308.0, 305.0},
		{291.0, 502.0},
		{76.0, 491.0},
	},
	[2]float64{20, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var SignMeevee Composition = Composition{
	"_sources/meevee_sign.png",
	[4][2]float64{
		{255.0, 71.0},
		{476.0, 67.0},
		{466.0, 207.0},
		{257.0, 202.0},
	},
	[2]float64{5, 5},
	[4]uint8{251, 251, 251, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

package composition

var SignAito Composition = Composition{
	"_sources/aito_sign.png",
	[4][2]float64{
		{33.0, 290.0},
		{476.0, 291.0},
		{476.0, 482.0},
		{31.0, 482.0},
	},
	[2]float64{3, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Kalam-Bold.ttf",
	[4]uint8{0, 0, 0, 255},
}

var ExclaimAkira Composition = Composition{
	"_sources/akira_exclaim.png",
	[4][2]float64{
		{40.0, 82.0},
		{252.0, 13.0},
		{292.0, 125.0},
		{93.0, 206.0},
	},
	[2]float64{10, 4},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var KestralAngy Composition = Composition{
	"_sources/kestral_angry.png",
	[4][2]float64{
		{78.0, 347.0},
		{450.0, 291.0},
		{476.0, 437.0},
		{102.0, 490.0},
	},
	[2]float64{10, 8},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var LynxCatch Composition = Composition{
	"_sources/lynx_catch.png",
	[4][2]float64{
		{44.0, 30.0},
		{444.0, 30.0},
		{444.0, 86.0},
		{45.0, 86.0},
	},
	[2]float64{5, 3},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var RykerCape Composition = Composition{
	"_sources/ryker_cape.png",
	[4][2]float64{
		{51.0, 303.0},
		{165.0, 279.0},
		{183.0, 364.0},
		{64.0, 389.0},
	},
	[2]float64{3, 5},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var VulanCrying Composition = Composition{
	"_sources/vulan_crying.png",
	[4][2]float64{
		{47.0, 16.0},
		{410.0, 44.0},
		{395.0, 224.0},
		{26.0, 203.0},
	},
	[2]float64{3, 4},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Fruktur-Regular.ttf",
	[4]uint8{0, 0, 0, 255},
}

var JfetAngry Composition = Composition{
	"_sources/jfet_angry.png",
	[4][2]float64{
		{3.25, 230.17},
		{338.25, 218.17},
		{341.75, 505.0},
		{10.25, 530.0},
	},
	[2]float64{10, 16},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{200, 0, 0, 255},
}

var SnepShow Composition = Composition{
	"_sources/snep_show.png",
	[4][2]float64{
		{28.0, 84.0},
		{204.0, 30.0},
		{231.0, 130.0},
		{68.0, 178.0},
	},
	[2]float64{5, 4},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

var FarfalleBelly Composition = Composition{
	"_sources/farfalle_belly.png",
	[4][2]float64{
		{82.0, 59.0},
		{366.0, 42.0},
		{388.0, 159.0},
		{104.0, 182.0},
	},
	[2]float64{5, 4},
	[4]uint8{255, 255, 255, 255},
	"_fonts/Roboto-Black.ttf",
	[4]uint8{0, 0, 0, 255},
}

package composition

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sticker-bot/internal/metrics"
	"time"
)

type Composition struct {
	SourceFilename      string
	CanvasControlPoints [4][2]float64
	CanvasPadding       [2]float64
	CanvasColor         [4]uint8
	FontFilename        string
	FontColor           [4]uint8
}

type ComposeRequest struct {
	SourcePNGBase64      string  `json:"source_png_base64"`
	CanvasControlPointAX float64 `json:"canvas_control_point_a_x"`
	CanvasControlPointAY float64 `json:"canvas_control_point_a_y"`
	CanvasControlPointBX float64 `json:"canvas_control_point_b_x"`
	CanvasControlPointBY float64 `json:"canvas_control_point_b_y"`
	CanvasControlPointCX float64 `json:"canvas_control_point_c_x"`
	CanvasControlPointCY float64 `json:"canvas_control_point_c_y"`
	CanvasControlPointDX float64 `json:"canvas_control_point_d_x"`
	CanvasControlPointDY float64 `json:"canvas_control_point_d_y"`
	CanvasPaddingWidth   float64 `json:"canvas_padding_width"`
	CanvasPaddingHeight  float64 `json:"canvas_padding_height"`
	CanvasColorRed       uint8   `json:"canvas_color_red"`
	CanvasColorGreen     uint8   `json:"canvas_color_green"`
	CanvasColorBlue      uint8   `json:"canvas_color_blue"`
	CanvasColorAlpha     uint8   `json:"canvas_color_alpha"`
	FontFileBase64       string  `json:"font_file_base64"`
	FontColorRed         uint8   `json:"font_color_red"`
	FontColorGreen       uint8   `json:"font_color_green"`
	FontColorBlue        uint8   `json:"font_color_blue"`
	FontColorAlpha       uint8   `json:"font_color_alpha"`
	Text                 string  `json:"text"`
}

type ComposeResponse struct {
	EncodedWebP string `json:"encoded_webp"`
	Error       string `json:"error"`
}

func (comp Composition) GetComposedImage(query string, composeURL string) []byte {
	ogImage, ogImageLoadErr := os.ReadFile(comp.SourceFilename)
	if ogImageLoadErr != nil {
		log.Print("Unable to load OG image")
		metrics.CompositionError("Image load")
		return []byte{}
	}
	ogFont, ogFontLoadErr := os.ReadFile(comp.FontFilename)
	if ogFontLoadErr != nil {
		log.Print("Unable to load OG font")
		metrics.CompositionError("Font load")
		return []byte{}
	}
	dataObj := ComposeRequest{
		SourcePNGBase64:      base64.StdEncoding.EncodeToString(ogImage),
		CanvasControlPointAX: comp.CanvasControlPoints[0][0],
		CanvasControlPointAY: comp.CanvasControlPoints[0][1],
		CanvasControlPointBX: comp.CanvasControlPoints[1][0],
		CanvasControlPointBY: comp.CanvasControlPoints[1][1],
		CanvasControlPointCX: comp.CanvasControlPoints[2][0],
		CanvasControlPointCY: comp.CanvasControlPoints[2][1],
		CanvasControlPointDX: comp.CanvasControlPoints[3][0],
		CanvasControlPointDY: comp.CanvasControlPoints[3][1],
		CanvasPaddingWidth:   comp.CanvasPadding[0],
		CanvasPaddingHeight:  comp.CanvasPadding[1],
		CanvasColorRed:       comp.CanvasColor[0],
		CanvasColorGreen:     comp.CanvasColor[1],
		CanvasColorBlue:      comp.CanvasColor[2],
		CanvasColorAlpha:     comp.CanvasColor[3],
		FontFileBase64:       base64.StdEncoding.EncodeToString(ogFont),
		FontColorRed:         comp.FontColor[0],
		FontColorGreen:       comp.FontColor[1],
		FontColorBlue:        comp.FontColor[2],
		FontColorAlpha:       comp.FontColor[3],
		Text:                 query,
	}
	jsonStr, jsonErr := json.Marshal(dataObj)
	if jsonErr != nil {
		log.Print("Unable to serialize request")
		metrics.CompositionError("Serialization")
		return []byte{}
	}

	req, reqErr := http.NewRequest("POST", composeURL, bytes.NewBuffer(jsonStr))
	if reqErr != nil {
		log.Print("Unable to create request")
		metrics.CompositionError("Request creation")
		return []byte{}
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	client.Timeout = time.Second * 4
	resp, respErr := client.Do(req)
	if respErr != nil {
		log.Print("Unable to get response")
		metrics.CompositionError("Response fetch")
		return []byte{}
	}
	body, bodyErr := ioutil.ReadAll(resp.Body)
	if bodyErr != nil {
		log.Print("Unable to read body")
		metrics.CompositionError("Response read")
		return []byte{}
	}
	responseObj := ComposeResponse{}
	unmarshErr := json.Unmarshal(body, &responseObj)
	if unmarshErr != nil {
		log.Print("Unable to unmarshal response")
		metrics.CompositionError("Deserialization")
		return []byte{}
	}
	if len(responseObj.Error) > 0 {
		log.Print(responseObj.Error)
		metrics.CompositionError("Remote")
		return []byte{}
	}
	decodedBody, decodeErr := base64.StdEncoding.DecodeString(responseObj.EncodedWebP)
	if decodeErr != nil {
		log.Print("Unable to decode body")
		metrics.CompositionError("Image decode")
		return []byte{}
	}
	return decodedBody
}

func GetCompositions() []Composition {
	compositions := []Composition{
		//BakaFreem,
		BakaFreemRoboto,
		//BakaOggy,
		//BakaAzshara,
		//BakaDbleki,
		SignMeevee,
		SignRadar,
		//BakaDaxter,
		SignDusk,
		//SignAito,
		//ExclaimAkira,
		//KestralAngy,
		//LynxCatch,
		//RykerCape,
		//VulanCrying,
		JfetAngry,
		//SnepShow,
		FarfalleBelly,
		//AwooMorning,
		AwooAngry,
		//AwooScream,
	}
	return compositions
}

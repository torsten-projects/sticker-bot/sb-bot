package composition

var AwooMorning Composition = Composition{
	"_sources/awoo_comic_morning.png",
	[4][2]float64{
		{57.0, 19.0},
		{414.0, 18.0},
		{417.0, 163.0},
		{57.0, 170.0},
	},
	[2]float64{10, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/ComicSans-Regular.ttf",
	[4]uint8{0, 0, 0, 255},
}

var AwooAngry Composition = Composition{
	"_sources/awoo_comic_angry.png",
	[4][2]float64{
		{6.0, 0.0},
		{512.0, 0.0},
		{512.0, 124.0},
		{6.0, 124.0},
	},
	[2]float64{0, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/ComicSans-Regular.ttf",
	[4]uint8{0, 0, 0, 255},
}

var AwooScream Composition = Composition{
	"_sources/awoo_comic_scream.png",
	[4][2]float64{
		{0.0, 0.0},
		{457.0, 0.0},
		{457.0, 412.0},
		{0.0, 412.0},
	},
	[2]float64{0, 10},
	[4]uint8{255, 255, 255, 255},
	"_fonts/ComicSans-Regular.ttf",
	[4]uint8{0, 0, 0, 255},
}

package workers

import (
	"log"
	"sticker-bot/internal/config"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func getBotForToken(token string) *tg.BotAPI {
	bot, err := tg.NewBotAPI(token)
	if err != nil {
		return nil
	}
	return bot
}

func getWorkers() []*tg.BotAPI {
	botAPIs := make([]*tg.BotAPI, 0, 30)
	for _, workerKey := range config.Config.WorkerAPIKeys {
		workerBot := getBotForToken(workerKey)
		if workerBot != nil {
			botAPIs = append(botAPIs, workerBot)
		}
	}
	if len(botAPIs) == 0 {
		log.Fatal("Unable to load any worker keys")
	}
	return botAPIs
}

var allWorkers []*tg.BotAPI = getWorkers()

var lastWorkerIx int = 0

func GetNextWorker() *tg.BotAPI {
	nextWorker := allWorkers[lastWorkerIx]
	lastWorkerIx = (lastWorkerIx + 1) % len(allWorkers)
	return nextWorker
}

func GetAllWorkers() []*tg.BotAPI {
	return allWorkers
}

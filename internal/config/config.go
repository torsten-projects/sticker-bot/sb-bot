package config

import (
	"log"
	"os"
	"strconv"
	"strings"
)

type Configuration struct {
	MasterAPIKey     string
	WorkerAPIKeys    []string
	CachingChannelID int
	ComposerURL      string
}

var Config Configuration = GetConfiguration()

func GetConfiguration() Configuration {
	return Configuration{
		MasterAPIKey:     getOrFail("TG_BOT_API"),
		WorkerAPIKeys:    strings.Split(getOrFail("WORKER_API_KEYS"), ";"),
		CachingChannelID: intOrFail(getOrFail("CACHING_CHANNEL_ID")),
		ComposerURL:      getOrFail("COMPOSER_URL"),
	}
}

func getOrFail(key string) (value string) {
	value, ok := os.LookupEnv(key)
	if !ok {
		log.Fatalf("Unable to lookup environment variable '%v'!", key)
	}
	return
}

func intOrFail(str string) (value int) {
	value, err := strconv.Atoi(str)
	if err != nil {
		log.Fatalf("Unable to parse '%v' as an int: %v", value, err)
	}
	return
}

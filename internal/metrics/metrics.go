package metrics

import (
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type exposedMetrics struct {
	QueriesFailed         *prometheus.CounterVec
	QueriesCompleted      prometheus.Counter
	CompositionsFailed    *prometheus.CounterVec
	CompositionsCompleted prometheus.Counter
	CompositionTime       prometheus.Histogram
	CachingTime           prometheus.Histogram
	ResponseTime          prometheus.Histogram
	WorkerBotCount        prometheus.Gauge
}

var namespace string = "stickerbot"
var subsystem string = "bot"
var histogramBuckets []float64 = []float64{
	0.01, // 10 ms
	0.05, // 50 ms
	0.2,  // 200 ms
	1.0,  // 1 s
	2.0,  // 2 s
	4.0,  // 4 s
	7.0,  // 7 s
	10.0, // 10 s
}

var Metrics exposedMetrics = exposedMetrics{
	QueriesFailed: promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "queries_failed_total",
		Help:      "The total number of failed queries by error type",
	}, []string{
		"error",
	}),
	QueriesCompleted: promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "queries_completed_total",
		Help:      "The total number of completed queries",
	}),
	CompositionsFailed: promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "compositions_failed_total",
		Help:      "How many image compositions failed to retrieve",
	}, []string{
		"error",
	}),
	CompositionsCompleted: promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "compositions_completed_total",
		Help:      "How many image compositions successfuly retrieved",
	}),
	CompositionTime: promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "composition_time",
		Buckets:   histogramBuckets,
		Help:      "How long fetching composed images took",
	}),
	CachingTime: promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "caching_time",
		Buckets:   histogramBuckets,
		Help:      "How long uploading images to caching channel took",
	}),
	ResponseTime: promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "response_time",
		Buckets:   histogramBuckets,
		Help:      "How long responding to inline query took",
	}),
	WorkerBotCount: promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: subsystem,
		Name:      "worker_bot_count",
		Help:      "Number of active worker bots",
	}),
}

func Serve() {
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatalf("Unable to serve metrics: %v", err)
	}
}

func CompositionError(err string) {
	Metrics.CompositionsFailed.WithLabelValues(err).Inc()
}

func QueryError(err string) {
	Metrics.QueriesFailed.WithLabelValues(err).Inc()
}

func DurationSeconds(since time.Time) float64 {
	return float64(time.Since(since) / time.Second)
}

FROM docker.io/golang:1.18-alpine AS build
WORKDIR /build
RUN apk add --no-cache git
COPY . .
RUN apk add --no-cache ca-certificates
RUN CGO_ENABLED=0 go build -v -installsuffix 'static' ./cmd/bot

FROM scratch AS runtime
ENTRYPOINT [ "/bot" ]
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY _fonts /_fonts
COPY _sources /_sources
COPY --from=build /build/bot /

package main

import (
	"fmt"
	"log"
	"sort"
	"sticker-bot/internal/composition"
	"sticker-bot/internal/config"
	"sticker-bot/internal/metrics"
	"sticker-bot/internal/workers"
	"time"

	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/google/uuid"
)

type compositionResult struct {
	success    bool
	fileID     string
	orderingID int
}

func composeSend(bot *tg.BotAPI, orderingID int, query string, comp composition.Composition, resultChannel chan compositionResult) {
	timing_start := time.Now()
	bytes := comp.GetComposedImage(query, config.Config.ComposerURL)
	if len(bytes) == 0 {
		resultChannel <- compositionResult{
			success: false,
		}
		log.Printf("Unable to get composed image")
		return
	}
	// only observe timings on successful compositions
	metrics.Metrics.CompositionTime.Observe(metrics.DurationSeconds(timing_start))
	timing_start = time.Now()
	file := tg.FileBytes{
		Name:  "sticker.webp",
		Bytes: bytes,
	}
	newSticker := tg.NewSticker(-1001174651711, file)
	stickerResp, stickerErr := bot.Send(newSticker)
	if stickerErr != nil {
		resultChannel <- compositionResult{
			success: false,
		}
		metrics.CompositionError(fmt.Sprintf("Caching upload: %v", stickerErr))
		log.Printf("Unable to upload sticker to caching channel: %s", stickerErr)
		return
	}
	if stickerResp.Sticker == nil {
		resultChannel <- compositionResult{
			success: false,
		}
		metrics.CompositionError("Caching upload: no FileID")
		log.Printf("Upload to caching channel did not provide sticker response")
		return
	}
	fileId := stickerResp.Sticker.FileID
	metrics.Metrics.CachingTime.Observe(metrics.DurationSeconds(timing_start))
	resultChannel <- compositionResult{
		success:    true,
		fileID:     fileId,
		orderingID: orderingID,
	}
	metrics.Metrics.CompositionsCompleted.Inc()
}

func handleQuery(masterBot *tg.BotAPI, query *tg.InlineQuery) {
	log.Printf("Got inline query request from '%v - %v %v': '%v'", query.From.UserName, query.From.FirstName, query.From.LastName, query.Query)
	timing_start := time.Now()

	compositionResultChannel := make(chan compositionResult)
	compositions := composition.GetCompositions()

	for ix, comp := range compositions {
		go composeSend(workers.GetNextWorker(), ix, query.Query, comp, compositionResultChannel)
	}

	receivedResults := []compositionResult{}
	for range compositions {
		received := <-compositionResultChannel
		if received.success {
			receivedResults = append(receivedResults, received)
		}
	}

	sort.Slice(receivedResults[:], func(i, j int) bool {
		return receivedResults[i].orderingID < receivedResults[j].orderingID
	})

	inlineResults := []tg.InlineQueryResultCachedSticker{}
	for _, result := range receivedResults {
		inlineResults = append(inlineResults, tg.NewInlineQueryResultCachedSticker(uuid.NewString(), result.fileID, "sticker.webp"))
	}

	log.Printf("Received %v valid compositions", len(inlineResults))

	typedResults := make([]interface{}, len(inlineResults))
	for i, v := range inlineResults {
		typedResults[i] = v
	}

	inlineConf := tg.InlineConfig{
		InlineQueryID: query.ID,
		IsPersonal:    true,
		CacheTime:     0,
		Results:       typedResults,
	}
	iqResp, iqErr := masterBot.Request(inlineConf)
	if iqErr != nil {
		log.Printf("Got error from responding to inline query: %v", iqErr)
		metrics.QueryError(fmt.Sprintf("Responding to IQ: %s", iqErr))
	} else {
		log.Printf("Got response: %v", iqResp.Description)
		if len(typedResults) > 0 {
			metrics.Metrics.ResponseTime.Observe(metrics.DurationSeconds(timing_start))
			metrics.Metrics.QueriesCompleted.Inc()
		} else {
			metrics.QueryError("Zero compositions")
		}
	}
}

func main() {
	bot, err := tg.NewBotAPI(config.Config.MasterAPIKey)
	if err != nil {
		log.Fatalf("Unable to connect to TG: %s", err)
	}

	log.Printf("Main bot authorized as '%v'", bot.Self.UserName)

	for _, worker := range workers.GetAllWorkers() {
		log.Printf("Worker bot authorized as '%v'", worker.Self.UserName)
	}

	metrics.Metrics.WorkerBotCount.Set(float64(len(workers.GetAllWorkers())))

	go metrics.Serve()

	u := tg.NewUpdate(0)
	u.Timeout = 3
	update_channel := bot.GetUpdatesChan(u)
	for update := range update_channel {
		if query := update.InlineQuery; query != nil {
			go handleQuery(bot, query)
		}
	}
}
